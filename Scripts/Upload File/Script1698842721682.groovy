import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.site/')

WebUI.click(findTestObject('Object Repository/button_Masuk'))

WebUI.setText(findTestObject('Object Repository/set-text_email'), 'sdivo164@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/input_Kata                                 _98da12'), '0K40W4r8I27nhH1MCUAIwQ==')

WebUI.sendKeys(findTestObject('Object Repository/input_Kata                                 _98da12'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/i_Kontak_fas fa-user-alt'))

WebUI.click(findTestObject('Object Repository/a_My                                       _8994d2'))

WebUI.click(findTestObject('Object Repository/span_Profil'))

WebUI.click(findTestObject('Object Repository/a_Edit Profile'))

WebUI.uploadFile(findTestObject('Upload File'), 'C:\\Users\\Hp\\Downloads\\cat-1882807_960_720.webp')

WebUI.click(findTestObject('button_Save Changes'))

WebUI.verifyElementVisible(findTestObject('div_Berhasil'))

WebUI.closeBrowser()

