<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Upload File DDT</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>ddaa0a53-bf4b-409d-8bf4-871a2c9f601e</testSuiteGuid>
   <testCaseLink>
      <guid>e88daee9-4894-4438-a144-f069872c5805</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Upload File - DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>57654d2b-f9d1-475e-8bc9-e2e5ca9d0fa8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>57654d2b-f9d1-475e-8bc9-e2e5ca9d0fa8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>path</value>
         <variableId>ca71922b-aa55-4e66-8fed-728fb31d43f2</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
